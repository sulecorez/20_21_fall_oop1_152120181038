#include <iostream>
#include <fstream>
using namespace std;

int SumFunc(int numbers[], int numberOfNumbers);
int ProductFunc(int numbers[], int numberOfNumbers);
int SmallestFunc(int numbers[], int numberOfNumbers);

int main()
{
	int numberOfNumbers, numbers[30], sum, product, smallest;
	float average;
	fstream inputFile; // Creating file stream
	inputFile.open("input.txt", std::fstream::in); // Opening file as reading mode(in)

	if (!inputFile) // Program warns if it couldn't open the file
	{
		cout << "File open failed" << endl;
		return 0;
	}

	inputFile >> numberOfNumbers; // Reading first input and assigning that value to numberOfNumbers
	for (int i = 0; i < numberOfNumbers; i++) // Reading numbers one by one and assigning the numbers to array
	{
		inputFile >> numbers[i];
	}
	sum = SumFunc(numbers, numberOfNumbers);
	cout <<"Sum is " << sum;
	product = ProductFunc(numbers, numberOfNumbers);
	cout << endl << "Product is " << product;
	average = (float)sum / (float)numberOfNumbers;
	cout << endl << "Average is " << average;
	smallest = SmallestFunc(numbers, numberOfNumbers);
	cout << endl << "Smallest is " << smallest;


}

int SumFunc(int numbers[], int numberOfNumbers) //That function receives numbers array and numberOfNumbers, returns sum of all numbers
{
	int sum = 0;
	for (int i = 0; i < numberOfNumbers; i++) // In every loop,numbers add to sum 
	{
		sum = sum + numbers[i];
	}
	return sum;
}

int ProductFunc(int numbers[], int numberOfNumbers) //This function receives numbers array and numberOfNumbers, returns product of all numbers
{
	int product = 1;
	for (int i = 0; i < numberOfNumbers; i++) // In every loop, numbers are multiplied one by one
	{
		product = product * numbers[i];
	}
	return product;
}

int SmallestFunc(int numbers[], int numberOfNumbers) //This function receives numbers array and numberOfNumbers, returns the smallest of all numbers
{
	int smallest;
	smallest = numbers[0]; // Assuming the first number is the smallest
	for (int i = 0; i < numberOfNumbers; i++) // Comparing all the numbers with the smallest number
	{
		if (numbers[i] < smallest) // If this number is smaller than the smallest number, this will be the new smallest number
		{
			smallest = numbers[i];
		}
	}
	return smallest;
}
